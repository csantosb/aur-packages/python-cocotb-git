#+TITLE: Python-cocotb-git AUR package

[[https://gitlab.com/aur-packages/python-cocotb-git/-/commits/master][https://gitlab.com/aur-packages/python-cocotb-git/badges/master/pipeline.svg]]

[[https://docs.cocotb.org/en/latest/][Cocotb]] is a coroutine based cosimulation library for writing VHDL and Verilog
testbenches in Python. This [[https://wiki.archlinux.org/index.php/Arch_Build_System][PKGBUILD]] provides a way to build a package suitable
for [[https://www.archlinux.org/][Archlinux]].

This project lives [[https://gitlab.com/aur-packages/python-cocotb-git][here]]. The included =yml= file tests the [[https://gitlab.com/aur-packages/python-cocotb-git/pipelines][build]] process.
